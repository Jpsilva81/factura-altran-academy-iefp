package com.example.jpsilva.factura.controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.jpsilva.factura.R;
import com.example.jpsilva.factura.model.Produto;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText preco, quantidade;
    TextView total, totalIva, totalDescontos;
    Button adicionar;
    Spinner categoria;
    ArrayList<Produto> listaProdutos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();

        preco = findViewById(R.id.txtPreco);
        quantidade = findViewById(R.id.txtQuantidade);
        total = findViewById(R.id.txtTotal);
        totalIva = findViewById(R.id.txtTotalIva);
        totalDescontos = findViewById(R.id.txtTotalDescontos);
        adicionar = findViewById(R.id.btnOK);
        categoria = findViewById(R.id.spinCategoria);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.categorias, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categoria.setAdapter(adapter);

        adicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Produto p = new Produto();
                if (preco.getText().toString().equals("")){
                    preco.setError("Valor Vazio");
                } else if (quantidade.getText().toString().equals("")){
                    quantidade.setError("Valor Vazio");
                } else {
                    p.setPrecoUnitario(Double.parseDouble(preco.getText().toString()));
                    p.setQuantidade(Integer.parseInt(quantidade.getText().toString()));
                    p.setCategoria(categoria.getSelectedItem().toString().charAt(0));
                    listaProdutos.add(p);
                    calcularValores();

                    preco.setText("");
                    quantidade.setText("");
                }
            }
        });
    }

    private void calcularValores() {
        double t=0, tIva=0, tDescontos=0;
        for (int i = 0; i<listaProdutos.size();i++){
            double linha = listaProdutos.get(i).getPrecoUnitario() * listaProdutos.get(i).getQuantidade();
            double desconto = desconto(listaProdutos.get(i).getCategoria());
            t += linha * desconto;
            tDescontos += linha - (linha * desconto);
        }
        tIva = t * (1 + 0.23);

        total.setText(String.format("%.2f€",t));
        totalIva.setText(String.format("%.2f€",tIva));
        totalDescontos.setText(String.format("%.2f€",tDescontos));

    }

    private double desconto(char categoria) {
        double desconto = 0;
        switch (categoria){
            case 'A' : desconto = 1 - 0.05; break;
            case 'B' : desconto = 1 - 0.10; break;
            case 'C' : desconto = 1 - 0.17; break;
            default: desconto = 1;
        }
        return desconto;
    }


}
